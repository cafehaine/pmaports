# Maintainer: Caleb Connolly <caleb@connolly.tech>
# Co-Maintainer: Joel Selvaraj <jo@jsfamily.in>
# Stable Linux kernel with patches for SDM845 devices
# Kernel config based on: arch/arm64/configs/defconfig and sdm845.config

_flavor="postmarketos-qcom-sdm845"
pkgname=linux-$_flavor
pkgver=5.16.12
pkgrel=0
pkgdesc="Mainline Kernel fork for SDM845 devices"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/sdm845-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-nftables
	pmb:kconfigcheck-containers
	pmb:kconfigcheck-zram
	pmb:kconfigcheck-anbox
	pmb:kconfigcheck-iwd"
makedepends="bison findutils flex installkernel openssl-dev perl"

_repo="linux"
_config="config-$_flavor.$arch"
_tag="sdm845-5.16.12"

# Source
source="
	$_repo-$_tag.tar.gz::https://gitlab.com/sdm845-mainline/$_repo/-/archive/$_tag/$_repo-$_tag.tar.gz
	$_config
"
builddir="$srcdir/$_repo-$_tag"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	mkdir -p "$pkgdir"/boot

	install -Dm644 "$builddir/arch/$_carch/boot/Image.gz" \
		"$pkgdir/boot/vmlinuz"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
f30d0992c5b3cb3198841f71a94a2306425211601e3fa347b3e39780fd1be0b3becc2c5eee870744a2d7a5821c66937d594f3ccfe59f38ceb6acb2e21c205504  linux-sdm845-5.16.12.tar.gz
5fe621ec5413cc7a4408ca58e7599435fee0063582f7f50e4b8378bddb574b7ed63644d31778bc6660ff221331c0574fa14c02fd496ae15b7b6cb4a318f4ebfb  config-postmarketos-qcom-sdm845.aarch64
"
